
// Import tests
#[cfg(test)]
mod tests;

fn get_input(prompt: &str, possibleval: &[u8]) -> usize {
    println!("{prompt}");

    let mut input = String::from("");

    std::io::stdin()
        .read_line(&mut input)
        .expect("Failed to read input.");

    let parsed_input = input.trim().parse::<u8>().unwrap_or_else(|error| {
        eprintln!("Error - {:?}", error);
        return 0;
    });

    for i in possibleval {
       if parsed_input == *i {
           return *i as usize;
        }
    }

    println!("Incorrect input, expecting one of {:?}, try again", possibleval);
    return get_input(prompt, &possibleval);
}

fn player_turn(play_field: & mut [[u8;3];3], turn: bool, row: usize, column: usize) {
    if play_field[row][column] == 0 {
        if turn {play_field[row][column] = 1;}
        else {play_field[row][column] = 2;}
    }
    else {
        panic!("Error - Value at {row}:{column} have already been changed, indicates incorrect input to player_turn function. \n Variables for bug report: \n play_field: {:?} \n turn: {turn} \n row: {row} \n column: {column}", play_field);
        // play_field is kept separately to not cause an error
    }
}

// Check for win - inputs 0 if not win, 1 if Player 1 wins, 2 if Player 2 wins
fn is_win(play_field:[[u8;3];3]) -> u8 {
    // Check diagonals
    if (play_field[0][0] == play_field[1][1]) && (play_field[1][1] == play_field[2][2]) {
        return play_field[0][0];
    }
    if (play_field[0][2] == play_field[1][1]) && (play_field[1][1] == play_field[2][0]) {
        return play_field[0][2];
    }

    // Check rows
    for row in play_field {
        if (row[0] == row[1]) && (row[1] == row[2]) {
            return row[0];
        }
    }

    // Check columns
    if  (play_field[0][0] == play_field[1][0]) && (play_field[1][0] == play_field[2][0]) {
        return play_field[0][0];
    }

    if  (play_field[0][1] == play_field[1][1]) && (play_field[1][1] == play_field[2][1]) {
        return play_field[0][1];
    }

    if  (play_field[0][2] == play_field[1][2]) && (play_field[1][2] == play_field[2][2]) {
        return play_field[0][2];
    }

    return 0;
}

// Checks if all cells are filled
fn is_draw(play_field:[[u8;3];3]) -> bool {
    for row in play_field {
        for column in row {
            if column == 0 {
                return false;
            }
        }
    }
    true
}

// Print all cells
fn print_play_field(play_field: [[u8;3];3]) {
    for row in play_field {
        // println!(" [{:?}] [{:?}] [{:?}]", row[0], row[0], row[0]);
        for column in row {
            match column {
                0 => print!(" [ ] "), // indicates no one changed it
                1 => print!(" [x] "), // indicates first player changed it
                2 => print!(" [o] "), // indicates second player changed it
                _ => panic!("Error - play_field contains incorrect value \n Variables for bug report: \n play_field: {:?}", play_field)
            }

        }
        println!() // add newline
    }
    return;
}

// Part of turn that will be repeated if it's invalid'
fn make_turn(play_field: & mut [[u8;3];3], first_turn: bool) {
        print_play_field(*play_field);

        let possible_coordinates: [u8;3] = [1,2,3]; // Field is 3x3 so you can choose one of three for either row or column
        let row = get_input("Input row: ", &possible_coordinates)-1;
        let column = get_input("Input column: ", &possible_coordinates)-1;

        if play_field[row][column] == 0 {
            // because arrays are indexed from zero, substract one from get_input
            player_turn(play_field, first_turn, row, column);
        }
        else {
            println!("There is already something at that cell");
            make_turn(play_field, first_turn);
        }
}

// If game is won
fn game_win(player: u8) {
    println!("Player {player} wins!");
    return;
}

fn main() {
    let mut play_field: [[u8;3];3] = [[0,0,0],[0,0,0],[0,0,0]];

    println!("Hello, world!");

    print_play_field(play_field);

    let mut first_turn = true; // Indicates that it's turn of Player 1, otherwise assumed Player 2

    loop {
        if first_turn {println!("Player 1:");}
        else {println!("Player 2:");}

        make_turn(&mut play_field, first_turn);

        first_turn = !first_turn;

        match is_win(play_field) {
            1 => return game_win(1),
            2 => return game_win(2),
            _ => (),
        }

        if is_draw(play_field) {
            println!("Draw!");
            return;
        }
    }

}
