use super::*;

// Check that most board are correctly recognised as draws or wins
#[test]
fn results_test() {
    let board = [
        [2, 1, 2],
        [1, 2, 1],
        [1, 2, 1]
    ];
    assert_eq!(is_win(board),0);
    assert_eq!(is_draw(board),true);

    let board = [
        [1, 1, 1],
        [0, 0, 0],
        [2, 2, 0]
    ];

    assert_eq!(is_win(board),1);
    assert_eq!(is_draw(board),false);

    let board = [
        [1, 2, 1],
        [0, 2, 0],
        [2, 2, 2]
    ];
    assert_eq!(is_win(board),2);
    assert_eq!(is_draw(board),false);


    let board = [
        [2, 1, 1],
        [2, 2, 0],
        [2, 2, 2]
    ];
    assert_eq!(is_win(board),2);
    assert_eq!(is_draw(board),false);


    let board = [
        [2, 1, 1],
        [2, 2, 1],
        [1, 2, 1]
    ];
    assert_eq!(is_win(board),1);
    assert_eq!(is_draw(board),true);

    let board = [
        [2, 1, 2],
        [2, 2, 1],
        [2, 2, 1]
    ];
    assert_eq!(is_win(board),2);
    assert_eq!(is_draw(board),true);

    let board = [
        [2, 0, 1],
        [0, 1, 0],
        [1, 0, 2]
    ];
    assert_eq!(is_win(board),1);
    assert_eq!(is_draw(board),false);

}

// Check that simple turns give correct changes
#[test]
fn turn_test() {
    // Player 1
    let mut play_field: [[u8;3];3] = [[0,0,0],[0,0,0],[0,0,0]];
    player_turn(&mut play_field, true, 1, 1);
    assert_eq!(play_field,[[0,0,0],[0,1,0],[0,0,0]]);
    // Player 2
    player_turn(&mut play_field, false, 0, 1);
    assert_eq!(play_field,[[0,2,0],[0,1,0],[0,0,0]]);
}


// Checks that all possible values are printed without an error
#[test]
fn print_test() {
    for n in [0,1,2] as [u8;3] {
        print_play_field([[n,n,n],[n,n,n],[n,n,n]]);
    }
}
