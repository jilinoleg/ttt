# Tic-tac-toe

TTT is a small CLI tic-tac-toe game. It's basically a Rust rewrite of https://codeberg.org/jilinoleg/tttt made out of boredom while I was traveling by train.

There are several improvements compared to cpp version, but it's not that much as for now.

## Build
- Run with `cargo run`
- Build with `cargo build -r` (-r is for release version)

## Todo
- Add command line arguments (like --help)
- Add accesible mode (replace square brackets with screenreader-friendly text, will be enabled with --accesibility)
- Separate code to make it possible to reuse it in proper TUI or GUI versions.
- Add CPU player and singleplayer mode

## License
All the code is under MIT License.
